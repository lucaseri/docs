.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../../../definitions.rst

Aladeen - 0.1.0
###############

About
*****

The objective of this document is to provide basic introductory information
about included functionalities, known issues, instructions guidance for
the Aladeen release of the |main_project_name| project.

The |main_project_name| project is meant to serve as a solid base foundation
for products. It is not a standalone product itelf, but rather a starting
project for other projects and products.

Scope
*****

**Release codename**: Aladeen

**Release version**: 0.1.0

**Release timeframe**: 2020/11/15 .. 2021/04/12

The Objectives of the Release
-----------------------------

The purpose of this release is to provide the European |main_project_name|
project with a set of initial functionalities after the first code release
in September, 2020. This April release serves as a solid foundation upon
which we can share the entire 2021 roadmap of the project.

More details on the |main_project_name| goals can be found in the
:doc:`/overview/oniroproject-vision` document.

The List of Software Features Included
--------------------------------------

+------------------------------------------------------------------+--------------------------+
| **Functionalities**                                              | **Area**                 |
+==================================================================+==========================+
| Bitbake build infrastructure                                     | Build tools              |
+------------------------------------------------------------------+--------------------------+
| Continuous IP compliance and Openchain IP policy                 | FOSS compliance          |
+------------------------------------------------------------------+--------------------------+
| Linux 5.10                                                       | Kernel                   |
+------------------------------------------------------------------+--------------------------+
| Zephyr 2.5                                                       | Kernel                   |
+------------------------------------------------------------------+--------------------------+
| FreeRTOS (experimental)                                          | Kernel                   |
+------------------------------------------------------------------+--------------------------+
| GCC / LLVM cross toolchains                                      | Toolchain                |
+------------------------------------------------------------------+--------------------------+
| gdb / gdbserver debugging                                        | Tools                    |
+------------------------------------------------------------------+--------------------------+
| Qemu targets for Intel and ARM                                   | Simulators               |
+------------------------------------------------------------------+--------------------------+
| Build flavors and base / extra root fs images                    | Build tools and root fs  |
+------------------------------------------------------------------+--------------------------+
| Smart Panel Blueprint                                            | Reference Blueprint      |
+------------------------------------------------------------------+--------------------------+
| Continuous IP compliance and Openchain IP policy                 | FOSS compliance          |
+------------------------------------------------------------------+--------------------------+
| Release Bill of Material and FOSS compliance dashboard           | FOSS compliance          |
+------------------------------------------------------------------+--------------------------+
| Application Compatibility Test Suite                             | Testing and compatibility|
+------------------------------------------------------------------+--------------------------+
| CI / CD pipelines and DevSecOps public cloud infrastructure      | DevSecOps                |
+------------------------------------------------------------------+--------------------------+
| Web portal and communication assets including Twitch channel     | Marcom                   |
+------------------------------------------------------------------+--------------------------+
| Automated sphinx-based documentation pipeline                    | Documentation            |
+------------------------------------------------------------------+--------------------------+

Supported Hardware Platforms
----------------------------

+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+----------------------------------+
| Board (chipset)                                                                                                                      | Supported kernels   | Board documentation              |
+======================================================================================================================================+=====================+==================================+
| `Nitrogen (nRF52832 - Cortex-M4) <https://docs.zephyrproject.org/latest/boards/arm/96b_nitrogen/doc/index.html#boards-nitrogen>`__   | Zephyr              | :ref:`SupportedBoardNitrogen`    |
+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+----------------------------------+
| `Avenger96 (STM32 - Cortex-A7 & Cortex-M4) <https://docs.zephyrproject.org/latest/boards/arm/96b_avenger96/doc/index.html>`__        | Linux & Zephyr      |                                  |
+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+----------------------------------+
| `SBC-C61 (NXP i.MX 8M - Coretex-A53 & Cortex M4) <https://pdf.directindustry.com/pdf/seco-spa/sbc-c61/54043-913344.html>`__          | Linux               | :ref:`SupportedBoardSecoC61`     |
+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+----------------------------------+
| `SBC-B68-eNUC (Intel x86) <https://pdf.directindustry.com/pdf/seco-spa/sbc-b68-enuc/54043-913350.html>`__                            | Linux               | :ref:`SupportedBoardSecoB68`     |
+--------------------------------------------------------------------------------------------------------------------------------------+---------------------+----------------------------------+


Test Report
-----------

Not available for this release.

Installation
************

:ref:`Quick Build <OniroQuickBuild>` provides an example of how to build
the |main_project_name| project for a supported target. Visit the
:ref:`Hardware Support <HardwareSupport>` section for instructions on how to
build for other supported targets.

Due to project rename and reorganization of the repos Aladen release doesn't require
repo tool. Entire workspace is archived into sources tarball and can be prepared with:

.. code-block:: console

   $ tar -xvf asos-0.1.0.tar.xz
   $ cd asos-0.1.0

Known Issues
------------

The Avenger96 target (stm32mp1-av96) does not ship with firmware files for Bluetooth,
Wifi and the image processor by default. We are working on a solution to re-distribute
them during the normal image builds for this board.

Source Code Available
---------------------

Source code and GPG signatures:

`asos-0.1.0.tar.xz <https://obs.eu-de.otc.t-systems.com/files.ostc-eu.org/asos-0.1.0.tar.xz>`_

`asos-0.1.0.tar.xz.sig <https://obs.eu-de.otc.t-systems.com/files.ostc-eu.org/asos-0.1.0.tar.xz.sig>`_

The release is signed with the key:

.. code-block::

   -----BEGIN PGP PUBLIC KEY BLOCK-----

   mDMEYMoZMBYJKwYBBAHaRw8BAQdAJhMs58bN6YYu2v9xZcyIkSSKQdpRm61Cac7y
   FPuN8BK0VlN0ZWZhbiBTY2htaWR0IChBbGwgU2NlbmFyaW9zIE9TIFJlbGVhc2Ug
   U2lnbmluZyBLZXkgMjAyMCkgPHN0ZWZhbi5zY2htaWR0QGh1YXdlaS5jb20+iJYE
   ExYIAD4WIQQHOpqTQvKyQyHYvxd7YfaxNxtJNwUCYMoZMAIbAwUJAeEzgAULCQgH
   AgYVCgkICwIEFgIDAQIeAQIXgAAKCRB7YfaxNxtJN1DDAP4q1a4jJh0PVtF/OTRf
   AhbBb2R8pFx5WLTGgUBvyFNXvwD9HFZHJCH7z8OvcO+70r27J3Kbwn/COPsu4eZI
   x11UYwu4MwRgyhkwFgkrBgEEAdpHDwEBB0CBT/QrS+DquNFyzLFiwntmiz3ml553
   WAFapVYBooBY2Yh+BBgWCAAmFiEEBzqak0LyskMh2L8Xe2H2sTcbSTcFAmDKGTAC
   GyAFCQHhM4AACgkQe2H2sTcbSTdRCwD/cstokK68pKzdpVldJmAth54jQu6wJtEJ
   4h5ABbPs92gBAPMfzKk15b1c5GaXOCrduOx8EPX4okH9yE+v0UfESGkBuDgEYMoZ
   MBIKKwYBBAGXVQEFAQEHQH/9Ag5C0I+DL4zu09dT6Gp/Fgzvg5dTqxSLf/5TlBEd
   AwEIB4h+BBgWCAAmFiEEBzqak0LyskMh2L8Xe2H2sTcbSTcFAmDKGTACGwwFCQHh
   M4AACgkQe2H2sTcbSTesYAD+O0A0NMi6OnqO7Ozknpzf/+FgT5n/fYmbc1dbAzSp
   klgA/jeBSvrxjZ1V5QoptpWamjhVwbdWavCR5iDGcVa9aCkG
   =lKOe
   -----END PGP PUBLIC KEY BLOCK-----

Devops Infrastructure
*********************

To learn more about our approach to CI (Continuous Integration) strategy used for this release, please see:

:doc:`/ci/index` document

Contributions
*************

If you are a developer eager to know more details about |main_project_name| or
just an enthusiast with a patch proposal, you are welcome to participate to our |main_project_name|
ecosystem development. To do so, please sign-up using the process described below:

:doc:`/contributing/index` document

License
*******

Project manifest as well as project-specific meta-layers, recipes and
software packages are, unless specified otherwise, published under
Apache 2.0 license. The whole operating system built by users from the
project manifest is an aggregate comprised of many third-party components
or component groups, each subject to its own license conditions.

Official project release includes only the project manifest as well as
project-specific meta-layers, recipes.
Any reference binary image, build cache and any other build
artifacts are distributed as a convenience only and are not part of the
release itself.
