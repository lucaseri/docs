.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../definitions.rst

Security Policies
#################

This section describes the security policies and practices of |main_project_name|.

.. toctree::
   :maxdepth: 1

   cve_policy
   guide
